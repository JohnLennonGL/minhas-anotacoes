package com.jlngls.minhasanotacoes;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText anotacoes;
    private ImageButton botaoSalvar;
    private static final String ARQUIVO_PREFERENCIA = "Arquivo Preferencia";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        anotacoes = findViewById(R.id.anotacoesID);
        botaoSalvar = findViewById(R.id.buttonID);

        botaoSalvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedPreferences = getSharedPreferences(ARQUIVO_PREFERENCIA,0);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("texto",anotacoes.getText().toString());
                editor.commit();
                Toast.makeText(MainActivity.this, "Sua anotaçōes foram salvas", Toast.LENGTH_SHORT).show();


            }
        });

        SharedPreferences sharedPreferences = getSharedPreferences(ARQUIVO_PREFERENCIA,0);
        if(sharedPreferences.contains("texto")){
        String salve = sharedPreferences.getString("texto","sem texto");
        anotacoes.setText(salve);
    }

    }
}
